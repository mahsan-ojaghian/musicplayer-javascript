const image = document.querySelector("img");
const title = document.getElementById("title");
const artist = document.getElementById("artist");
const music = document.querySelector("audio");
const progressContainer = document.getElementById("progress-container");
const progress = document.getElementById("progress");
const currentTimeEl = document.getElementById('current-time');
const durationEl = document.getElementById('duration');
const prevBtn = document.getElementById("prev");
const playBtn = document.getElementById("play");
const nextBtn = document.getElementById("next");
//musics
const songs = [
    {
        name : "Jacinto-1",
        displayName : "Electric Chill Machine",
        artist : "Jacinto Design"
    },
    {
        name : "Jacinto-2",
        displayName : "Seven Nation Army(Remix)",
        artist : "Jacinto Design"
    },
    {
        name : "Jacinto-3",
        displayName : "GoodNight, Disco Queen",
        artist : "Jacinto Design"
    },
    {
        name : "Metric-1",
        displayName : "Front Row",
        artist : "Metric/Jacinto Design"
    }
];
//check if playing
let isPlaying = false;
//play
function playSong(){
    playBtn.classList.replace("fa-play","fa-pause");
    playBtn.setAttribute("title","pause");
    isPlaying = true;
    music.play()
}
//pause
function pauseSong(){
    playBtn.classList.replace("fa-pause","fa-play");
    playBtn.setAttribute("title","play");
    isPlaying = false;
    music.pause()
}
//play or pause addEventListener
playBtn.addEventListener("click", () => (isPlaying ? pauseSong() : playSong()));

//Update Dom
function loadSong(song){
title.textContent = song.displayName;
artist.textContent = song.artist;
music.src = `music/${song.name}.mp3`;
image.src = `img/${song.name}.jpg`;
}
//current song
let songIndex = 0;
// previous Song
function prevSong(){
    songIndex--;
    if(songIndex < 0){
        songIndex = songs.length -1;
    }
    loadSong(songs[songIndex]);
    playSong();
}
//nextSong
function nextSong(){
    songIndex++;
    if(songIndex > songs.length -1){
        songIndex = 0;
    };
    loadSong(songs[songIndex]);
    playSong();
}

//on Load Select First Song
loadSong(songs[songIndex]);
//update Progress Bar
function updateProgressBar(e){
  if  (isPlaying){
    const { duration , currentTime} = e.srcElement;
    //Update progress bar width
const progressPercent = (currentTime / duration) * 100;
progress.style.width = `${progressPercent}%`;
//calculate display for duration
const durationMinutes = Math.floor(duration / 60);
let durationSeconds = Math.floor(duration % 60);
if(durationSeconds < 10){
    durationSeconds = `0${durationSeconds}`;
};
//Delay
if(durationSeconds){
durationEl.textContent = `${durationMinutes}:${durationSeconds}`;
}
// calculate display for current
const currentMinutes = Math.floor(currentTime / 60);
let currentSeconds = Math.floor(currentTime % 60);
if(currentSeconds < 10){
    currentSeconds = `0${currentSeconds}`;
}
currentTimeEl.textContent = `${currentMinutes}:${currentSeconds}`;

  }
}
// set progress bar
function setProgressBar(e){
    const width = this.clientWidth;
    const clickX = e.offsetX;
    const { duration} = music;
    music.currentTime = (clickX / width) * duration;
}

// Event Listeners
prevBtn.addEventListener('click',prevSong);
nextBtn.addEventListener('click',nextSong);
music.addEventListener('ended' , nextSong)
music.addEventListener('timeupdate' , updateProgressBar);
progressContainer.addEventListener('click',setProgressBar);